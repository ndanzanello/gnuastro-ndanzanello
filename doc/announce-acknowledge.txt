Alphabetically ordered list to acknowledge in the next release.

Mark Calabretta
Sepideh Eskandarlou
Raul Infante-Sainz
Alberto Madrigal
Juan Miro
Carlos Morales Socorro
Sylvain Mottet
Francois Ochsenbein
Samane Raji
Zahra Sharbaf
Ignacio Trujillo





Copyright (C) 2015-2021 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU Free Documentation License, Version 1.3 or any later
version published by the Free Software Foundation; with no Invariant
Sections, with no Front-Cover Texts, and with no Back-Cover Texts.
